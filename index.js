// Discussion #1

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// Using "keyup" event

/* 
    txtFirstName.addEventListener("keyup", (event) => {
        spanFullName.innerHTML = txtFirstName.value;
    });
    txtFirstName.addEventListener("keyup", (event) => {
        // The document where our JS is connected
        console.log(event.target);
        // The value that our "keyup" event listened
        console.log(event.target.value);
    });
 */

// Discussion 2
const updateFullName = () => {
    let firstName = txtFirstName.value,
        lastName = txtLastName.value;

    spanFullName.innerHTML = `${firstName} ${lastName} `;
};

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);
